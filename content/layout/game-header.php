<!DOCTYPE html>
<head>
    <title> <?=$gamename?> ~ kids.moosader.com</title>

    <link rel="stylesheet" href="content/css/pagestyle.css">
    <script src="content/js/jquery/jquery-3.3.1.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One|Indie+Flower|Shrikhand" rel="stylesheet">
</head>
<body>
    <section class="main">
        <div class="page-title clearfix">
            <img src="content/page-gfx/moosehead.png" class="logo">
            <h1 class="page-title">Moosader Kids</h1>
        </div>

        <div class="clear"></div>
