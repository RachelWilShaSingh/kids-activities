$( document ).ready( function() {

    firstTimeRight = 0;
    totalQuestions = 0;
    alreadyAnswered = false;
    
    function Random( min, max ) {
        return Math.floor( Math.random() * (max - min + 1) ) + min;
    }

    function Percentage( num, denom ) {
        return parseFloat( num / denom ).toFixed(2) * 100;
    }

    function GenerateQuestion() {
        num1 = Random( 10, 99 );
        num2 = Random( 10, 99 );
        addsub = Random( 1, 2 );
        alreadyAnswered = false;
        
        $( "#check-answer" ).val( "Check answer" );
        $( "#right-wrong" ).css( "display", "none" );
        $( "#wrong-guy" ).css( "display", "none" );
        $( "#right-guy" ).css( "display", "none" );

        if ( addsub == 1 ) {
            answer = num1 + num2;
            $( "#math" ).html( "+" );
        }
        else {
            if ( num1 < num2 ) {
                var c = num1;
                num1 = num2;
                num2 = c;
            }
            answer = num1 - num2;
            $( "#math" ).html( "-" );
        }

        $("#num1").html( num1 );
        $("#num2").html( num2 );
        $("#answer").val( "" );
    }

    function CheckAnswer() {
        $( "#right-wrong" ).fadeIn( "fast" );
        $( "#wrong-guy" ).css( "display", "none" );
        $( "#right-guy" ).css( "display", "none" );
        
        if ( $( "#answer" ).val() == answer ) {
            $( "#right-wrong" ).html( $( "#answer" ).val() + " is right!" );
            $( "#right-guy" ).fadeIn( "fast" );
            $( "#check-answer" ).val( "Next question" );

            if ( alreadyAnswered == false ) {
                // First try
                firstTimeRight += 1;
            }
        }
        else {
            $( "#right-wrong" ).html( $( "#answer" ).val() + " is wrong! Try again!" );
            $( "#wrong-guy" ).fadeIn( "fast" );
        }

        if ( alreadyAnswered == false )
            totalQuestions += 1;
        alreadyAnswered = true;

        UpdateStats();
    }

    function UpdateStats() {
        $( "#stat-correct" ).html( Percentage( firstTimeRight, totalQuestions ) + "%" );
        $( "#stat-questions" ).html( totalQuestions );
    }

    $("#check-answer").click( function() {
        if ( $( "#check-answer" ).val() == "Check answer" )
            CheckAnswer();
        else
            GenerateQuestion();
    } );

    GenerateQuestion();


} );
