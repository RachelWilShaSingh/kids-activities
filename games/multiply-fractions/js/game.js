$( document ).ready( function() {

    firstTimeRight = 0;
    totalQuestions = 0;
    alreadyAnswered = false;
    
    function Random( min, max ) {
        return Math.floor( Math.random() * (max - min + 1) ) + min;
    }

    function Percentage( num, denom ) {
        return parseFloat( num / denom ).toFixed(2) * 100;
    }

    function GenerateQuestion() {
        num1 = Random( 1, 12 );
        denom1 = Random( 1, 12 );
        num2 = Random( 1, 12 );
        denom2 = Random( 1, 12 );
        
        alreadyAnswered = false;
        
        $( "#check-answer" ).val( "Check answer" );
        $( "#right-wrong" ).css( "display", "none" );
        $( "#wrong" ).css( "display", "none" );
        $( "#right" ).css( "display", "none" );

        answerNum = num1 * num2;
        answerDenom = denom1 * denom2;
        $( "#math" ).html( "x" );

        $("#num1").html( num1 );
        $("#denom1").html( denom1 );
        $("#num2").html( num2 );
        $("#denom2").html( denom2 );
        $("#answerNum").val( "" );
        $("#answerDenom").val( "" );
    }

    function CheckAnswer() {
        $( "#right-wrong" ).fadeIn( "fast" );
        $( "#wrong" ).css( "display", "none" );
        $( "#right" ).css( "display", "none" );

        var answered = $( "#answerNum" ).val() + "/" + $( "#answerDenom" ).val();
        
        if ( $( "#answerNum" ).val() == answerNum && $( "#answerDenom" ).val() == answerDenom ) {
            $( "#right-wrong" ).html( answered + " is right!" );
            $( "#right" ).fadeIn( "fast" );
            $( "#check-answer" ).val( "Next question" );

            if ( alreadyAnswered == false ) {
                // First try
                firstTimeRight += 1;
            }
        }
        else {
            $( "#right-wrong" ).html( answered + " is wrong! Try again!" );
            $( "#wrong" ).fadeIn( "fast" );
        }

        if ( alreadyAnswered == false )
            totalQuestions += 1;
        alreadyAnswered = true;

        UpdateStats();
    }

    function UpdateStats() {
        $( "#stat-correct" ).html( Percentage( firstTimeRight, totalQuestions ) + "%" );
        $( "#stat-questions" ).html( totalQuestions );
    }

    $("#check-answer").click( function() {
        if ( $( "#check-answer" ).val() == "Check answer" )
            CheckAnswer();
        else
            GenerateQuestion();
    } );

    GenerateQuestion();


} );
