$( document ).ready( function() {

    firstTimeRight = 0;
    totalQuestions = 0;
    alreadyAnswered = false;

    animals = [ "ant", "bear", "bee", "bird", "butterfly", "cat", "chicken", "cow", "crab", "crocodile", "dog", "dolphin", "elephant", "frog", "kangaroo", "lion", "llama", "monkey", "moose", "owl", "penguin", "pig", "rabbit", "seal", "skunk", "snake", "squirrel", "turkey", "turtle", "whale" ];
    
    function Random( min, max ) {
        return Math.floor( Math.random() * (max - min + 1) ) + min;
    }

    function Percentage( num, denom ) {
        return parseFloat( num / denom ).toFixed(2) * 100;
    }

    function GenerateQuestion() {
        numToCount = Random( 1, 15 );
        animal = Random( 0, animals.length - 1 );
        alreadyAnswered = false;
        
        $( "#submit-answer" ).val( "Check Answer" );
        $( "#wrong" ).fadeOut( "fast" );
        $( "#right" ).fadeOut( "fast" );

        $( "#question-text" ).html( "How many <strong>" + animals[ animal ] + "s</strong> are there?" );

        $("#user-answer").val( "" );

        $( ".counting-animal" ).remove();
        for ( var i = 0; i < numToCount; i++ ) {
            var string = "";
            if ( i % 5 == 0 && i != 0 ) { string += "<br>" }
            string += "<img src='assets/images/" + animals[ animal ] + ".png' class='counting-animal'>";
            $( ".how-many-items" ).append( string );
        }
        
        $( "#submit-answer" ).addClass( "check-answer" );
        $( "#submit-answer" ).removeClass( "new-question" );
        $( "#question-text" ).removeClass( "wrong" );
        $( "#question-text" ).removeClass( "right" );
        $( "#question-text" ).addClass( "neutral" );
    }

    function CheckAnswer() {
        $( "#wrong" ).css( "display", "none" );
        $( "#right" ).css( "display", "none" );

        var userAnswer = $( "#user-answer" ).val();
        
        if ( userAnswer == numToCount ) {
            $( "#question-text" ).html( userAnswer + " is right!" );
            $( "#right" ).fadeIn( "fast" );
            $( "#submit-answer" ).val( "Next question" );
            $( "#submit-answer" ).removeClass( "check-answer" );
            $( "#submit-answer" ).addClass( "new-question" );
            $( "#question-text" ).addClass( "right" );
            $( "#question-text" ).removeClass( "neutral" );

            if ( alreadyAnswered == false ) {
                // First try
                firstTimeRight += 1;
            }
        }
        else {
            $( "#question-text" ).html( userAnswer + " is wrong!<br>Try again!" );
            $( "#wrong" ).fadeIn( "fast" );
            $( "#question-text" ).addClass( "wrong" );
            $( "#question-text" ).removeClass( "neutral" );
        }

        if ( alreadyAnswered == false )
            totalQuestions += 1;
        alreadyAnswered = true;

        UpdateStats();
    }

    function UpdateStats() {
        $( "#stat-correct" ).html( Percentage( firstTimeRight, totalQuestions ) + "%" );
        $( "#stat-questions" ).html( totalQuestions );
    }

    $("#submit-answer").click( function() {
        if ( $( "#submit-answer" ).val() == "Check Answer" )
            CheckAnswer();
        else
            GenerateQuestion();
    } );
    
    GenerateQuestion();


} );
